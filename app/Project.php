<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [
        'name',
        'description',
        'project_manager_id'
    ];

    public function projectManager()
    {
        // many to one
        return $this->belongsTo(User::class);
    }
}
