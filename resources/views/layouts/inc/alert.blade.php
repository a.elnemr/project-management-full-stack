@if ($errors->any())
    @foreach ($errors->all() as $error)
        <div class="container">
            <div class="alert alert-danger">
                {{ $error }}
            </div>
        </div>
    @endforeach

@endif
<div class="container">
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div> <!-- end .flash-message -->
</div>
