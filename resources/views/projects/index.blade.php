@extends('layouts.app')

@section('title', 'All Projects')

@section('nav')
    @include('layouts.inc.nav')
@stop

@section('content')
    <div class="card" style="padding: 15px">
        <div class="card-body">
            <h1>Projects</h1>
            <a href="{{ route('project.create') }}" class="btn btn-success">Create</a>
        </div>

        <table class="table table-bordered">
            <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Project Manager</th>
                <th>Created At</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($projects as $project)
                <tr>
                    <td>{{ $project->id }}</td>
                    <td>{{ $project->name }}</td>
                    <td>{{ $project->projectManager->name }}</td>
                    <td>{{ $project->created_at }}</td>
                    <td>
                        <a href="{{ route('project.show', $project) }}" class="btn btn-warning">Show</a>
                        <a href="{{ route('project.edit', $project) }}" class="btn btn-info">Edit</a>
                        <form style="display: inline-block" action="{{ route('project.destroy', $project) }}" method="post">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>

        </table>
        {!! $projects->links() !!}
    </div>
@endsection
