<div class="form-group">
    <label for="name">Name</label>
    <input type="text"
           id="name"
           name="name"
           value="{{ isset($project)? $project->name:old('name') }}"
           class="form-control">
</div>
<div class="form-group">
    <label for="project_manager_id">Name</label>
    <select name="project_manager_id" id="project_manager_id" class="form-control">
        <option value=>Select Project Manager</option>
        @foreach($users as $user)
            @if(isset($project) && ($user->id === $project->project_manager_id))
                <option selected value="{{ $user->id }}">{{ $user->name }}</option>
            @else
                <option value="{{ $user->id }}">{{ $user->name }}</option>
            @endif
        @endforeach
    </select>
</div>
<div class="form-group">
    <label for="description">Description</label>
    <textarea name="description"
              id="description"
              class="form-control"
              cols="30"
              rows="10">{!! isset($project)? $project->description:old('description') !!}</textarea>
</div>
