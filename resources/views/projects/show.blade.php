@extends('layouts.app')

@section('title', 'Show Project')

@section('nav')
    @include('layouts.inc.nav')
@endsection

@section('content')
    <div class="card" style="padding: 15px">
        <div class="card-body">
            <h1>Project: {{ $project->name }} </h1>
            <h2>Project Manager: {{ $project->projectManager->name }} </h2>
            <p>
                {!! $project->description !!}
            </p>
            <a href="{{ route('project.edit', $project) }}" class="btn btn-info">Edit</a>

        </div>
    </div>
@endsection
