@extends('layouts.app')

@section('title', 'New Project')

@section('nav')
    @include('layouts.inc.nav')
@endsection

@section('content')
    <div class="card" style="padding: 15px">
        <div class="card-body">
            <h1>Edit Project</h1>
        </div>

        <form action="{{ route('project.update', $project) }}" method="post">
            @csrf
            @method('PUT')
            @include('projects._form')

            <button class="btn btn-success">Save</button>
            <a href="{{ route('project.index') }}" class="btn btn-info">Cancel</a>

        </form>

    </div>
@endsection
