@extends('layouts.app')

@section('title', 'All Users')

@section('nav')
    @include('layouts.inc.nav')
@stop

@section('content')
    <div class="card">
        <div class="card card-body">
            <h1>All Users</h1>

            <ul>
                @foreach($users as $user)
                    <li>{{ $user->name }} | {{ $user->email }}</li>
                @endforeach
            </ul>
        </div>
    </div>
@stop
