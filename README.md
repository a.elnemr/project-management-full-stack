## Download the project
 
Go to Hard Disk an open bash here and type
`git clone https://gitlab.com/a.elnemr/project-management-full-stack`

## Config database

create new database using phpmyadmin for example "project_management_full_stack"

## Run the project

In Project root type

`composer install`

## Config database connection 

open `.env` file and change 

`DB_CONNECTION=mysql`
`DB_HOST=127.0.0.1`
`DB_PORT=3306`
`DB_DATABASE=project_management_full_stack`
`DB_USERNAME=root`
`DB_PASSWORD=`

## Open the project

`php artisan serve`