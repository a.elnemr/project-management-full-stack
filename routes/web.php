<?php

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/admin', 'HomeController@index')->name('admin');

Route::get('/admin/user', 'HomeController@allUsers')->name('users.index');

Route::resource('/admin/project', 'ProjectController');
